package w05;

import java.util.*;
import java.util.Calendar;

public class StateHolidays {

    private static List<Calendar> list;

    public static List<Calendar> getInstance() {
        fullList();
        return list;
    }

    private static void fullList() {
        ArrayList<Calendar> arrayList = new ArrayList<Calendar>();

        Calendar newYear = Calendar.getInstance();
        newYear.set(2015, Calendar.JANUARY, 1);
        arrayList.add(newYear);

        Calendar Cris = Calendar.getInstance();
        Cris.set(2015, Calendar.JANUARY, 7);
        arrayList.add(Cris);

        Calendar womanDay = Calendar.getInstance();
        womanDay.set(2015, Calendar.MARCH, 8);
        arrayList.add(womanDay);

        Calendar workHardDay = Calendar.getInstance();
        workHardDay.set(2015, Calendar.MAY, 1);
        arrayList.add(workHardDay);

        Calendar victory = Calendar.getInstance();
        victory.set(2015, Calendar.MAY, 9);
        arrayList.add(victory);

        Calendar constitution = Calendar.getInstance();
        constitution.set(2015, Calendar.JUNE, 28);
        arrayList.add(constitution);

        Calendar independence = Calendar.getInstance();
        independence.set(2015, Calendar.AUGUST, 24);
        arrayList.add(independence);

        list = arrayList;
    }
}
