package w05;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Calendar")
public class Calendar extends javax.servlet.http.HttpServlet {

    long hitCounter;

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        PrintWriter out = new PrintWriter(response.getWriter());

        int year = Integer.parseInt(request.getParameter("year"));
        int month = Integer.parseInt(request.getParameter("month")) - 1;

        CalendarInfo info = new CalendarInfo();
        int column = info.getColumnQuantity(year, month);
        int row = 7;
        int dayInMonth = 1;

        out.println("Year: " + year + ", month: " + month + 1);
        out.println("<table align='center' border = '1'>");
        for (int i = 0; i < column; i++) {
            out.println("<tr>");
            for (int j = 0; j < row; j++) {
                if ((i == 0 && j < info.rowFirstDay(year, month)) || (dayInMonth > info.getDayInMonth(year, month))) {
                    out.println("<td> </td>");
                } else {
                    if (info.isHoliday(year, month, dayInMonth)) {
                        out.println("<td align='center'> <font color='#FF0000'> " + dayInMonth++ + "</font> </td>");
                    } else {
                        out.println("<td align=\"center\"> <font color='#000000'> " + dayInMonth++ + "</font> </td>");
                    }
                }
            }
            out.println("</tr>");
        }
        out.println("</table>");

        out.println("<p> Hit Counter : " + (++hitCounter) + "<br>");

        out.close();
    }

    @Override
    public void init() throws ServletException {
        super.init();
        hitCounter = 0;
    }
}
