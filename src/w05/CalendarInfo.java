package w05;

import java.util.Calendar;

public class CalendarInfo {
    public static final int AMENDMENT_DAYS_WEEK = 2;
    public static final int SUNDAY = 6;
    public static final int FIRST = 1;

    private java.util.Calendar day;

    public CalendarInfo() {
    }

    public int getColumnQuantity(int year, int month) {
        day = java.util.Calendar.getInstance();
        day.set(year, month, FIRST);
        java.util.Calendar dayLast = java.util.Calendar.getInstance();
        dayLast.set(year, month, day.getActualMaximum(Calendar.DAY_OF_MONTH));

        return dayLast.get(Calendar.WEEK_OF_YEAR) - day.get(Calendar.WEEK_OF_YEAR) + 1;
    }

    public int getDayInMonth(int year, int month) {
        day = java.util.Calendar.getInstance();
        day.set(year, month, FIRST);

        return day.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public int rowFirstDay(int year, int month) {
        day = java.util.Calendar.getInstance();
        day.set(year, month, FIRST);
        if (day.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {return SUNDAY;}

        return day.get(Calendar.DAY_OF_WEEK) - AMENDMENT_DAYS_WEEK;
    }

    public boolean isHoliday(int year, int month, int d) {
        day = java.util.Calendar.getInstance();
        day.set(year, month, d);

        return ((day.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) ||
                (day.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) ||
                isStateHoliday(day));
    }

    public boolean isStateHoliday(Calendar day) {
        for (Calendar holiday : StateHolidays.getInstance()) {
            if ((day.get(Calendar.MONTH) == holiday.get(Calendar.MONTH)) &&
                    (day.get(Calendar.DAY_OF_MONTH) == holiday.get(Calendar.DAY_OF_MONTH))) {
                return true;
            }
        }
        return false;
    }
}
