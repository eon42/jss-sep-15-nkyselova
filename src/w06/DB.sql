/*
SQLyog Community v12.02 (64 bit)
MySQL - 5.7.10-log : Database - contacts
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`contacts` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `contacts`;

/*Table structure for table `call` */

DROP TABLE IF EXISTS `call`;

CREATE TABLE `call` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `contact_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `call_ibfk_1` (`id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `call_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `call` */

insert  into `call`(`id`,`description`,`contact_id`) values (1,'Home',5),(2,'Home',7),(3,'Work',7);

/*Table structure for table `contact` */

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `phone_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `phone_id` (`phone_id`),
  CONSTRAINT `contact_ibfk_1` FOREIGN KEY (`phone_id`) REFERENCES `phone` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `contact` */

insert  into `contact`(`id`,`name`,`phone_id`) values (4,'Ivanov',1),(5,'Igor',3),(6,'Egor',4),(7,'Sergey',5),(8,'Michael',2);

/*Table structure for table `email` */

DROP TABLE IF EXISTS `email`;

CREATE TABLE `email` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(50) NOT NULL,
  `contact_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email_ibfk_1` (`contact_id`),
  CONSTRAINT `email_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `email` */

insert  into `email`(`id`,`address`,`contact_id`) values (2,'ivanov@gmail.com',4),(3,'ivanov@mail.ru',4),(4,'igor@gmail.com',5),(5,'egor@gmail.com',6),(6,'egor@ya.ru',6),(7,'michael@ya.ru',8),(8,'michael@ukr.net',8);

/*Table structure for table `meeting` */

DROP TABLE IF EXISTS `meeting`;

CREATE TABLE `meeting` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `title` varchar(20) NOT NULL,
  `time` time NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `meeting` */

insert  into `meeting`(`id`,`description`,`title`,`time`,`date`) values (1,'Organisation','First','12:00:00','2016-01-14'),(2,'Morning','Dayly','09:00:00','0000-00-00');

/*Table structure for table `meeting-contact` */

DROP TABLE IF EXISTS `meeting-contact`;

CREATE TABLE `meeting-contact` (
  `contact_id` bigint(20) unsigned NOT NULL,
  `meeting_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`contact_id`,`meeting_id`),
  KEY `meeting-contact_ibfk_2` (`meeting_id`),
  CONSTRAINT `meeting-contact_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`),
  CONSTRAINT `meeting-contact_ibfk_2` FOREIGN KEY (`meeting_id`) REFERENCES `meeting` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `meeting-contact` */

insert  into `meeting-contact`(`contact_id`,`meeting_id`) values (4,1),(5,1),(6,1),(4,2),(6,2),(7,2);

/*Table structure for table `phone` */

DROP TABLE IF EXISTS `phone`;

CREATE TABLE `phone` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `phone_number` bigint(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `phone` */

insert  into `phone`(`id`,`phone_number`) values (1,11111111),(2,2222),(3,333333),(4,44),(5,5);

/*Table structure for table `reminder` */

DROP TABLE IF EXISTS `reminder`;

CREATE TABLE `reminder` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `time` time NOT NULL,
  `description` varchar(50) NOT NULL,
  `meeting_id` bigint(20) unsigned DEFAULT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `meeting_id` (`meeting_id`),
  CONSTRAINT `reminder_ibfk_2` FOREIGN KEY (`meeting_id`) REFERENCES `meeting` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `reminder` */

insert  into `reminder`(`id`,`time`,`description`,`meeting_id`,`date`) values (1,'08:00:00','First meeting',1,'2016-01-14'),(2,'08:00:00','Every day meeting',2,'2016-01-16');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
